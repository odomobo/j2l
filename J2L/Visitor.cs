﻿using System;
using J2L.Grammar;
using System.Reflection.Emit;
using Antlr4.Runtime.Misc;
using System.Diagnostics.SymbolStore;
using Antlr4.Runtime;
using System.Collections.Generic;

namespace J2L {
    sealed class Visitor : J2ParserBaseVisitor<object> {
        private readonly Compiler _compiler;
        private readonly ILGenerator _ilg;
        private readonly bool _debugging;
        private readonly ISymbolDocumentWriter _document;
        private readonly ITokenStream _tokenStream;
        private readonly Dictionary<string, LocalBuilder> _locals;

        public Visitor(Compiler compiler) {
            _compiler = compiler;
            _ilg = compiler.Ilg;
            _debugging = compiler.Debugging;
            _document = compiler.Document;
            _tokenStream = compiler.TokenStream;
            _locals = new Dictionary<string, LocalBuilder>();
        }

        private void Debug(ParserRuleContext context) {
            // generate debugging information
            if (_debugging)
                _ilg.MarkSequencePoint(_document, context.Start.Line, context.Start.Column + 1, context.Stop.GetFinalLine(_tokenStream), context.Stop.GetFinalColumn(_tokenStream) + 1);
        }

        public override object VisitDeclareStatement([NotNull] J2Parser.DeclareStatementContext context) {
            // TODO: only debug when it's a compound declaration and assignment
            //Debug(context); // writes debugging info, if applicable

            string identifier = context.id.Text;
            if (_locals.ContainsKey(identifier)) {
                throw new Exception("Identifier already defined"); // TODO: better error message, correct exception class
            }

            Type type = Helpers.LookupType(context.type.Text);
            LocalBuilder local = _ilg.DeclareLocal(type);
            local.SetLocalSymInfo(identifier);
            
            _locals[identifier] = local;

            // TODO: perform assignment here, when it's a compound declaration and assignment

            return null;
        }

        public override object VisitAssignmentStatement([NotNull] J2Parser.AssignmentStatementContext context) {
            Debug(context); // writes debugging info, if applicable

            string identifier = context.id.Text;
            LocalBuilder local;
            if (!_locals.TryGetValue(identifier, out local)) {
                throw new Exception("Identifier not yet defined"); // TODO: better error message, correct exception class
            }

            Visit(context.value);
            // TODO: ensure types are correct
            _ilg.Emit(OpCodes.Stloc, local.LocalIndex);
            return null;
        }

        public override object VisitPrintStatement([NotNull] J2Parser.PrintStatementContext context) {
            Debug(context); // writes debugging info, if applicable

            VisitChildren(context);
            _ilg.Emit(OpCodes.Call,
                     typeof(Console).GetMethod("WriteLine",
                     new Type[] { typeof(int) }));
            return null;
        }

        public override object VisitPlusMinus([NotNull] J2Parser.PlusMinusContext context) {
            VisitChildren(context);
            if (context.op.Text == "+")
                _ilg.Emit(OpCodes.Add);
            else if (context.op.Text == "-")
                _ilg.Emit(OpCodes.Sub);
            else
                throw new Exception("Bad plus minus"); // TODO: better error message, correct exception class
            return null;
        }

        public override object VisitTimesDivide([NotNull] J2Parser.TimesDivideContext context) {
            VisitChildren(context);
            if (context.op.Text == "*")
                _ilg.Emit(OpCodes.Mul);
            else if (context.op.Text == "/")
                _ilg.Emit(OpCodes.Div);
            else
                throw new Exception("Bad times divide"); // TODO: better error message, correct exception class
            return null;
        }

        public override object VisitExponent([NotNull] J2Parser.ExponentContext context) {
            Visit(context.left);
            // convert int to double
            _ilg.Emit(OpCodes.Conv_R8);

            Visit(context.right);
            // convert int to double
            _ilg.Emit(OpCodes.Conv_R8);

            // perform exponentiation
            _ilg.Emit(OpCodes.Call,
                     typeof(Math).GetMethod("Pow",
                     new Type[] { typeof(double), typeof(double) }));

            // convert resulting double to int
            _ilg.Emit(OpCodes.Conv_I4);
            return null;
        }

        public override object VisitInteger([NotNull] J2Parser.IntegerContext context) {
            int number = int.Parse(context.value.Text);
            _ilg.Emit(OpCodes.Ldc_I4, number);
            return null;
        }

        public override object VisitIdentifier([NotNull] J2Parser.IdentifierContext context) {
            string identifier = context.id.Text;
            LocalBuilder local;
            if (!_locals.TryGetValue(identifier, out local)) {
                throw new Exception("Identifier not yet defined"); // TODO: better error message, correct exception class
            }

            _ilg.Emit(OpCodes.Ldloc, local.LocalIndex);

            // TODO: return type information
            return null;
        }
    }
}
