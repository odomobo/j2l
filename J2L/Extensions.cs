﻿using Antlr4.Runtime;

namespace J2L {
    static class Extensions {
        public static int GetFinalColumn(this IToken token, ITokenStream tokenStream) {
            IToken nextToken = tokenStream.Get(token.TokenIndex + 1);
            return nextToken.Column;
        }

        public static int GetFinalLine(this IToken token, ITokenStream tokenStream) {
            IToken nextToken = tokenStream.Get(token.TokenIndex + 1);
            return nextToken.Line;
        }
    }
}
