﻿namespace J2L {
    sealed class Program {
        private const bool IsDynamic = false;
        private const bool Debugging = true;
       
        static void Main(string[] args) {
            // TODO: argument parsing
            string inputFile = args[0];
            new Compiler(inputFile, IsDynamic, Debugging).Execute();
        }
    }
}
