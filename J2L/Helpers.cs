﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace J2L {
    static class Helpers {
        private static readonly Dictionary<string, Type> _typeLookup = new Dictionary<string, Type> {
            {"int", typeof(int)},
            {"bool", typeof(bool)},
        };

        public static Type LookupType(string typeStr) {
            Type type;
            if (_typeLookup.TryGetValue(typeStr, out type)) {
                return type;
            } else {
                throw new Exception("Could not find type"); // TODO: better error message, correct exception class
            }
        }
    }
}
