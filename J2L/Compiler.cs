﻿using Antlr4.Runtime;
using J2L.Grammar;
using System;
using System.Diagnostics;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using static J2L.Grammar.J2Parser;

namespace J2L {
    sealed class Compiler {
        public string InputFile { get; private set; }
        public bool Debugging { get; private set; }
        public ILGenerator Ilg { get; private set; }
        public ISymbolDocumentWriter Document { get; private set; }
        public ITokenStream TokenStream { get; private set; }
        public static readonly string AssemblyName = "Foo";
        public static readonly string ClassName = "Program";
        public static readonly string MethodName = "Main";

        private bool _isDynamic;

        public Compiler(string inputFile, bool isDynamic, bool debugging) {
            InputFile = inputFile;
            _isDynamic = isDynamic;
            Debugging = debugging;
        }

        public void Execute() {
            string outputFile = Path.ChangeExtension(InputFile, "exe");

            AssemblyName an = new AssemblyName();
            an.Name = AssemblyName;
            AppDomain ad = AppDomain.CurrentDomain;

            AssemblyBuilder ab;
            if (_isDynamic)
                ab = ad.DefineDynamicAssembly(an, AssemblyBuilderAccess.Run);
            else
                ab = ad.DefineDynamicAssembly(an, AssemblyBuilderAccess.Save);

            if (Debugging) {
                Type daType = typeof(DebuggableAttribute);
                ConstructorInfo daCtor = daType.GetConstructor(new Type[] { typeof(DebuggableAttribute.DebuggingModes) });
                var cabArgs = new object[] { DebuggableAttribute.DebuggingModes.DisableOptimizations
                                           | DebuggableAttribute.DebuggingModes.Default };
                CustomAttributeBuilder cab = new CustomAttributeBuilder(daCtor, cabArgs);
                ab.SetCustomAttribute(cab);
            }

            ModuleBuilder mb;
            if (_isDynamic)
                mb = ab.DefineDynamicModule(an.Name, Debugging);
            else
                mb = ab.DefineDynamicModule(an.Name, outputFile, Debugging);

            if (Debugging)
                Document = mb.DefineDocument(InputFile, Guid.Empty, Guid.Empty, Guid.Empty);

            TypeBuilder tb = mb.DefineType(string.Format("{0}.{1}", AssemblyName, ClassName),
                TypeAttributes.Public | TypeAttributes.Class);

            MethodBuilder fb = tb.DefineMethod(MethodName,
                MethodAttributes.Public |
                MethodAttributes.Static,
                typeof(void), new Type[] { typeof(string[]) });
            // hardcode args parameter name
            fb.DefineParameter(1, ParameterAttributes.None, "args");

            Ilg = fb.GetILGenerator();

            // use ANTLR to create IL code
            using (StreamReader fileStream = new StreamReader(InputFile)) {
                AntlrInputStream inputStream = new AntlrInputStream(fileStream);

                J2Lexer lexer = new J2Lexer(inputStream);
                TokenStream = new CommonTokenStream(lexer);
                J2Parser parser = new J2Parser(TokenStream);

                CompileUnitContext ctx = parser.compileUnit();
                new Visitor(this).Visit(ctx);
            }

            // return after method
            Ilg.Emit(OpCodes.Ret);

            // seal the type
            Type t = tb.CreateType();

            if (_isDynamic) {
                MethodInfo info = t.GetMethod(MethodName);
                info.Invoke(null, new object[] { new string[] { "" } });
                Console.ReadLine();
            } else {
                ab.SetEntryPoint(fb, PEFileKinds.ConsoleApplication);
                ab.Save(outputFile);
            }
        }
    }
}