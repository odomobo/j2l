## J2L ##
*Josh's Second Language*

This project is to create a simple programming language, compiler, the tools requires to use it. The lexer/parser uses ANTLR4, and the compiler uses Reflection.Emit.

## Setup ##
This project requires Visual Studio with Visual C#. Ensure that you have Java installed, as this is needed by ANTLR4. Note that the Java SDK is not required.
