parser grammar J2Parser;
options { tokenVocab=J2Lexer; }

compileUnit
	: (statement)* EOF
	;

statement
	: PRINT LPAREN expr RPAREN SEMICOLON # PrintStatement
	| type=TYPE id=IDENTIFIER SEMICOLON # DeclareStatement
	| id=IDENTIFIER ASSIGNMENT value=expr SEMICOLON # AssignmentStatement
	;

expr
	: LPAREN expr RPAREN # Parens
	| <assoc=right> left=expr EXP right=expr # Exponent
	| left=expr op=(MULT | DIV) right=expr # TimesDivide
	| left=expr op=(PLUS | MINUS) right=expr # PlusMinus
	| value=INTEGER # Integer
	| id=IDENTIFIER # Identifier
	;
