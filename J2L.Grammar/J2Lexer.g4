lexer grammar J2Lexer;

PLUS: '+';
MINUS: '-';
MULT: '*';
DIV: '/';
EXP: '^';
LPAREN: '(';
RPAREN: ')';
SEMICOLON: ';';
TYPE: 'int'
//	| 'bool'
    ;
PRINT: 'print';

ASSIGNMENT: '=';

INTEGER: [1-9][0-9]*;
IDENTIFIER: [a-zA-Z_][a-zA-Z0-9_]*;

WS
	: [ \t\r\n]+ -> channel(HIDDEN)
	;
